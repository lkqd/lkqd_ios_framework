//
//  LkqdDelegate.swift
//  Created by noah evans on 8/6/16.
//  Copyright © 2016 lkqd. All rights reserved.
//

import Foundation

public protocol LkqdDelegate: class
{
    func lkqdAdEnded(ad: LKQDAD)
    func lkqdEventReceived(ad: LKQDAD, event: String)
    func lkqdAdWebviewError(ad: LKQDAD, didFailLoadWithError error: NSError?)
    func lkqdAdTimeOut(ad: LKQDAD)
}