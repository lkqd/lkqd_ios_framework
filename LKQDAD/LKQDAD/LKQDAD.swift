//
//  LKQDAD.swift
//  Created by noah evans on 8/4/16.
//  Copyright © 2016 lkqd. All rights reserved.
//

import AdSupport
import MapKit

public class LKQDAD:NSObject, UIWebViewDelegate, CLLocationManagerDelegate
{
    //required view controller to show the web view
    weak var lkqdController:UIViewController?
    
    //user sets lkqdDelegate to handle events and callbacks from this class
    public weak var lkqdDelegate: LkqdDelegate?
    
    //pid and sid values identify accounts for ad deliver and are unique for each lkqd account
    var _pid:Int!
    var _sid:Int!
    
    //used for determining how much lkqd ad does with events
    var _automationLevel: Int?
    
    //---internal variables---
    //web view to show the ad and load lkqd.html page
    var lkqdWebView:UIWebView?
    
    //used for ad load timeout when initAd() is called
    weak var _timer: NSTimer?
    var _adLoadTimeoutTime:Double = 10.0
    
    //control flags for preventing multiple calls to ad functions
    var _initAdCalled: Bool?
    var _startAdCalled: Bool?
    var _adLoadedCalled: Bool?
    
    //used for finding location for tracking
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    let locationManager = CLLocationManager()
    
    //used for bitrate of video only when automation is at level 1 or higher
    var _desiredBitRate: NSNumber = 600;
    
    //initialize the lkqd ad 
    //pass in the view controller -- the lkqd ad webview will be added to this viewcontroller.view
    //pid and sid are unique values for each app: https://wiki.lkqd.com/display/PUBINT/iOS+SDK
    //automation level is used to determine how much control the user has over events
    public init(theController:UIViewController, pid:Int, sid:Int, desiredBitRate:NSNumber = 600, automationLevel:Int = 1)
    {
        super.init()
        
        lkqdController = theController
        _pid = pid;
        _sid = sid;
        _desiredBitRate = desiredBitRate;
        _automationLevel = automationLevel
        if(_automationLevel > 2)
        {
            _automationLevel = 2;
        }
        if(_automationLevel < 0)
        {
            _automationLevel = 0;
        }
        
        _initAdCalled = false;
        _startAdCalled = false;
        _adLoadedCalled = false;
        
        //if we are allowed to get location, then start updating location, otherwise just load web view
        //the locationManager() function will call makeWebViewRequest() when it gets the location
        if(CLLocationManager.locationServicesEnabled())
        {
            switch(CLLocationManager.authorizationStatus())
            {
                case .NotDetermined, .Restricted, .Denied:
                    print("No access to location services")
                    makeWebViewRequest()
                case .AuthorizedAlways, .AuthorizedWhenInUse:
                    print("location services are allowed")
                    locationManager.delegate = self;
                    locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    locationManager.startUpdatingLocation()
            }
        }
        else
        {
            print("Location services are not enabled")
            makeWebViewRequest()
        }
    }
    
    private func makeWebViewRequest()
    {
        print("preparing web view")
        //get app specific data
        let infoDictionary: NSDictionary = NSBundle.mainBundle().infoDictionary as NSDictionary!
        let appName: NSString = infoDictionary.objectForKey("CFBundleName") as! NSString
        let version: NSString = infoDictionary.objectForKey("CFBundleShortVersionString") as! NSString
        let bundleID = NSBundle.mainBundle().bundleIdentifier!
        var strIDFA : String = "No IDFA"
        if ASIdentifierManager.sharedManager().advertisingTrackingEnabled
        {
            strIDFA = ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
        }
        print("IDFA = \(strIDFA)")
        print("Name \(appName)")
        print("version \(version)")
        print("bundleID \(bundleID)")
        
        //get the url to load lkqd.html
        let indexHTMLPath: String = "https://ad.lkqd.net/sdk/lkqd.html"
        
        //get time to pass for cachebuster
        let currentDateTime = CACurrentMediaTime();
        
        //build the query string to send app data for tracking
        var queryString: String = "?appname=\(appName)";
        queryString = queryString + "&idfa=\(strIDFA)";
        queryString = queryString + "&appversion=\(version)";
        queryString = queryString + "&bundleid=\(bundleID)";
        queryString = queryString + "&latitude=\(latitude)";
        queryString = queryString + "&longitude=\(longitude)";
        queryString = queryString + "&pid=\(_pid)";
        queryString = queryString + "&sid=\(_sid)";
        queryString = queryString + "&sdk=iOS";
        queryString = queryString + "&cb=\(currentDateTime)";
        print(queryString)
        let str: String = indexHTMLPath + queryString
        let queryUrl = NSURL(string: str);
        let request = NSURLRequest(URL: queryUrl!)
        
        //create new instance of lkqdWebView each time
        lkqdWebView = UIWebView()
        lkqdWebView?.frame = (lkqdController?.view.frame)!; //make the webview the size of the controller view
        lkqdWebView?.delegate = self; //need this class to listen to web events from web view
        lkqdWebView?.allowsInlineMediaPlayback = true; //required to play ads automatically
        lkqdWebView?.mediaPlaybackRequiresUserAction = false; //required to play ads automatically
        lkqdWebView?.loadRequest(request) //load the url
        
        print("web view loading")
    }
    
    //called to start timeout for initAd() call in case nothing loads
    func resetInitAdTimer()
    {
        _timer?.invalidate()
        let nextTimer = NSTimer.scheduledTimerWithTimeInterval(_adLoadTimeoutTime, target: self, selector: #selector(LKQDAD.handleAdTimeout(_:)), userInfo: nil, repeats: false)
        _timer = nextTimer
    }
    
    //called on initAd() timeout to let lkqdDelegate know ad timed out and remove the timer
    func handleAdTimeout(timer: NSTimer)
    {
        _timer?.invalidate()
        lkqdDelegate?.lkqdAdTimeOut(self)
        //do whatever you want when idle after certain period of time
    }
    
    //all vpaid events pass through here before being relayed to user
    func handleVpaidEvent(vpaidEvent: String)
    {
        print("handleVpaidEvent in lkqdAd: \(vpaidEvent)")
        
        //handle event locally and do whatever needed for this lkqdAd instance
        if(vpaidEvent == "AdStopped" || vpaidEvent == "AdError")
        {
            removeSubview()
        }
        
        //called after initAd() has finished
        if(vpaidEvent == "AdLoaded")
        {
            _timer?.invalidate() //cancel the timeout for initAd()
            _adLoadedCalled = true; //once this is true, we can start setting values on the vpaid ad unit
        }
        
        //this should be called from javascript when the orientation changes
        //resizes the webview frame to the new view controller frame width/height
        if(vpaidEvent == "AdSizeChange")
        {
            lkqdWebView?.frame = (lkqdController?.view.frame)!;
        }
        
        //turn on Moat analytics once the page has loaded
        if(vpaidEvent == "webViewLoaded")
        {
            let moatOK: Bool = LQDMoatBootstrap.injectDelegateWrapper(lkqdWebView!) //required for MOAT ANALYTICS
            print("moatOK = ");
            print(moatOK);
        }
        
        /*
         Automation Level handling here
         If automationLevel != 0, then this class will handle some of the vpaid events
         Default is 1 which means the user will only have to call startAd() once they receive AdLoaded event
         Level 2 means the ad will automatically load and start ASAP if nothing fails
         Level 0 would be the user controlling each step and responding to webViewLoaded and AdLoaded themselves
        */
        
        //automationLevel=1
        //will do everything but call startAd() for the user
        if(_automationLevel! > 0)
        {
            if(vpaidEvent == "webViewLoaded")
            {
                print("webViewLoaded AUTO START")
                autoInit();
            }
        }
        
        //automationLevel=2
        //will play the ad as soon as possible
        if(_automationLevel! > 1)
        {
            if(vpaidEvent == "AdLoaded")
            {
                print("AdLoaded AUTO START")
                startAd();
            }
        }

        //LKQDAD has done all it needs to with the event...pass the event on to the lkqdDelegate to handle how it wants
        lkqdDelegate?.lkqdEventReceived(self, event: vpaidEvent)
    }
    
    //this function is called when automationLevel is set to 1 or 2
    //apparently initAd() has to be called with a slight delay for calling js on the webpage
    private func autoInit()
    {
        initAd((lkqdController?.view.frame.width)!, height: (lkqdController?.view.frame.height)!, viewMode: "normal", desiredBitrate: _desiredBitRate, creativeData: "", environmentVars: "")
    }
    
    //this function will be called when AdStopped or AdError is fired from the ad
    private func removeSubview()
    {
        print("Start remove lkqd subview")
        _initAdCalled = false;
        _startAdCalled = false;
        _adLoadedCalled = false;
        
        lkqdWebView?.removeFromSuperview()
        lkqdDelegate?.lkqdAdEnded(self)
    }
    
    //////////////////////////////////////////////////////////
    ///////////////////DELEGATE FUNCTIONS/////////////////////
    //////////////////////////////////////////////////////////
    
    //CLLocationManagerDelegate Method
    public func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        manager.stopUpdatingLocation()
        print("locationManager fail with error \(error)");
    }
    
    //CLLocationManagerDelegate Method
    public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        print("LATITUDE LONGITUDE")
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        latitude = coord.latitude
        longitude = coord.longitude
        print(coord.latitude)
        print(coord.longitude)
        manager.stopUpdatingLocation()
        makeWebViewRequest()
    }
    
    
    //UIWebViewDelegate function
    public func webView(webView: UIWebView, didFailLoadWithError error: NSError?)
    {
        print("Webview fail with error \(error)");
        //just in case web view fails to load call this so the user can know web view errored
        //i dont expect this to be used ever since the only page load is local
        lkqdDelegate?.lkqdAdWebviewError(self, didFailLoadWithError: error)
    }
    
    //UIWebViewDelegate function
    public func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType)-> Bool
    {
        print("loading request: \(request)")
        let url: String = request.URL!.absoluteString
        //do things here
        if let scheme = request.URL?.scheme
        {
            if scheme == "vpaid"
            {
                //vpaid commands are passed as a url request
                //process the command and find the first query param to get the event
                //other values can be passed but are not necessary yet
                let query = request.URL?.query
                let fullQueryArray: Array = query!.componentsSeparatedByString("&")
                var vpaidEvent: String = "";//fullQueryArray[0];
                for (index, value) in fullQueryArray.enumerate()
                {
                    let valueArray: Array = value.componentsSeparatedByString("=")
                    if(valueArray.count == 2)
                    {
                        if(valueArray[1] == "VPAID_EVENT")
                        {
                            vpaidEvent = valueArray[0];
                            print("Item \(index + 1): \(valueArray[0])")
                        }
                        else
                        {
                            //ignore extra values on iOS?
                            //getters can be handled through js
                        }
                    }
                }
                
                print("we got a vpaid request:  \(vpaidEvent)")
                
                if(vpaidEvent != "")
                {
                    handleVpaidEvent(vpaidEvent)
                }
                return false;
            }
            else
            {
                if(url.containsString("lkqd.html") || url.containsString("about:blank"))
                {
                    //these should be the only requests that aren't ad click thru
                }
                else
                {
                    //open the ad url in safari
                    UIApplication.sharedApplication().openURL(request.URL!)
                    
                    //deny the request to load in the webview so that the ad doesn't take over the app
                    return false;
                }
            }
        }
        return true;
    }
    
    //UIWebViewDelegate function
    public func webViewDidStartLoad(webView: UIWebView)
    {
        //print("Webview started Loading something")
        //this gets called on any request in the webview...ignore
    }
    
    //UIWebViewDelegate function
    public func webViewDidFinishLoad(webView: UIWebView) {
        
        let state = webView.stringByEvaluatingJavaScriptFromString("document.readyState")
        if(state == "complete")
        {
            //print("Webview did finish load")
            //lkqdDelegate should receive an event "webViewLoaded" in the vpaidEventReceived
            //do stuff there for when the page is finished loading and ready to call initAd()
        }
    }
    
    /*
        //////////////////////VPAID interface implementation////////////////////////
        The following functions are added just to maintain vpaid protocol. 
        A lot will not be necessary.
        All functions and their use cases can be found here:
        https://www.iab.com/wp-content/uploads/2015/06/VPAID_2_0_Final_04-10-2012.pdf
    */
    
    public func initAd(width : NSNumber, height : NSNumber, viewMode : String = "normal", desiredBitrate : NSNumber = 600, creativeData : String = "", environmentVars : String = "")
    {
        //prevent user from calling initAd multiple times
        if(_initAdCalled! == false)
        {
            _initAdCalled = true;
            //print("init lkqd ad")
            //print(width)
            //print(height)
            //print(viewMode)
            //print(desiredBitrate)
            //print(creativeData)
            //print(environmentVars)
        }
        else
        {
            return;
        }
        
        //just in case init ad doesn't complete start a timer here for timing out initAd() call
        resetInitAdTimer()
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("initAd('\(width)','\(height)','\(viewMode)','\(desiredBitrate)','\(creativeData)','\(environmentVars)')")
    }
    
    public func startAd()
    {
        print("starting lkqd ad")
        
        //prevent user from calling startAd multiple times
        if(_startAdCalled! == false)
        {
            _startAdCalled = true;
        }
        else
        {
            return;
        }
        
        lkqdController?.view.addSubview(lkqdWebView!)
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("startAd()")
    }
    
    public func resizeAd(width : CGFloat, height : CGFloat, viewMode : String = "normal")
    {
        //should only be calling resize ad once we know the ad is loaded
        if(_adLoadedCalled!)
        {
            print("resizing ad")
            print(width)
            print(height)
            print(viewMode)
            
            //resizing ad will resize the frame to new width/height and default position
            //right now it is defaulting to 0,0 for position to top left corner but this can be changed if needed
            lkqdWebView?.frame = CGRectMake(0, 0, width, height)
            lkqdWebView?.stringByEvaluatingJavaScriptFromString("resizeAd('\(width)','\(height)','\(viewMode)')")
        }
    }
    
    public func stopAd()
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("stopAd()")
    }
    
    public func pauseAd()
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("pauseAd()")
    }
    
    public func resumeAd()
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("resumeAd()")
    }
    
    public func expandAd()
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("expandAd()")
    }
    
    public func collapseAd()
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("collapseAd()")
    }
    
    public func skipAd()
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("skipAd()")
    }
    
    public func getAdLinear() -> Bool
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdLinear()")
        {
            if(returnedString == "true")
            {
                return true;
            }
        }
        return false;
    }
    
    public func getAdExpanded() -> Bool
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdExpanded()")
        {
            if(returnedString == "true")
            {
                return true;
            }
        }
        return false;
    }
    
    public func getAdRemainingTime() -> NSNumber
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdRemainingTime()")
        {
            return Double(returnedString)!
        }
        return -1;
    }
    
    public func getAdVolume() -> NSNumber
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdVolume()")
        {
            return Double(returnedString)!
        }
        return -1;
    }
    
    //value from 0-1 with 0 being mute and 1 being 100% volume
    public func setAdVolume(value: NSNumber)
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("setAdVolume('\(value)')")
    }
    
    //typically send "2.0" although this is already done on webview loading, anyway
    public func handshakeVersion(playerVPAIDVersion: String)
    {
        lkqdWebView?.stringByEvaluatingJavaScriptFromString("handshakeVersion('\(playerVPAIDVersion)')")
    }
    
    //width of ad usually an integer like 320
    public func getAdWidth() -> NSNumber
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdWidth()")
        {
            return Double(returnedString)!
        }
        return -1;
    }
    
    //height of ad usually an integer like 480
    public func getAdHeight() -> NSNumber
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdHeight()")
        {
            return Double(returnedString)!
        }
        return -1;

    }
    
    //whether or not an ad is skippable
    public func getAdSkippableState() -> Bool
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdSkippableState()")
        {
            if(returnedString == "true")
            {
                return true;
            }
        }
        return false;
    }
    
    //total length of time for the ad (15.19 seconds for example)
    public func getAdDuration() -> NSNumber
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdDuration()")
        {
            return Double(returnedString)!
        }
        return -2;
    }
    
    
    public func getAdCompanions() -> String
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdCompanions()")
        {
            return returnedString;
        }
        return "";
    }
    
    public func getAdIcons() -> Bool
    {
        if let returnedString = lkqdWebView?.stringByEvaluatingJavaScriptFromString("getAdIcons()")
        {
            if(returnedString == "true")
            {
                return true;
            }
        }
        return false;
    }
}

